///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "bird.hpp"

using namespace std;

namespace animalfarm {

const string Bird::speak(){
   return string ("Tweet");
}

void Bird::printInfo() {
   Animal::printInfo();
   cout<<"   Feather Color = [" << colorName(featherColor) << "]" <<endl;
   //cout<<"   is Migratory = [" << (isMigratory ? "True" : "False") << "]"  << endl; }
   cout<<"   Is Migratory = [" << boolalpha << isMigratory << "]"  << endl; }

}
